using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using LibraryWebApp.Models;

namespace LibraryWebApp.Migrations.Library
{
    [DbContext(typeof(LibraryContext))]
    partial class LibraryContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("LibraryWebApp.Models.AspNetRoleClaims", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 450);

                    b.HasKey("Id");
                });

            modelBuilder.Entity("LibraryWebApp.Models.AspNetRoles", b =>
                {
                    b.Property<string>("Id")
                        .HasAnnotation("MaxLength", 450);

                    b.Property<string>("ConcurrencyStamp");

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasAnnotation("Relational:Name", "RoleNameIndex");
                });

            modelBuilder.Entity("LibraryWebApp.Models.AspNetUserClaims", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 450);

                    b.HasKey("Id");
                });

            modelBuilder.Entity("LibraryWebApp.Models.AspNetUserLogins", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasAnnotation("MaxLength", 450);

                    b.Property<string>("ProviderKey")
                        .HasAnnotation("MaxLength", 450);

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 450);

                    b.HasKey("LoginProvider", "ProviderKey");
                });

            modelBuilder.Entity("LibraryWebApp.Models.AspNetUserRoles", b =>
                {
                    b.Property<string>("UserId")
                        .HasAnnotation("MaxLength", 450);

                    b.Property<string>("RoleId")
                        .HasAnnotation("MaxLength", 450);

                    b.HasKey("UserId", "RoleId");
                });

            modelBuilder.Entity("LibraryWebApp.Models.AspNetUsers", b =>
                {
                    b.Property<string>("Id")
                        .HasAnnotation("MaxLength", 450);

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("Address");

                    b.Property<string>("ConcurrencyStamp");

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("Phone");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecondName");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasAnnotation("Relational:Name", "EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .HasAnnotation("Relational:Name", "UserNameIndex");
                });

            modelBuilder.Entity("LibraryWebApp.Models.Book", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Author");

                    b.Property<int?>("CategoryId");

                    b.Property<string>("City");

                    b.Property<int?>("Count");

                    b.Property<DateTime?>("DateAdded")
                        .HasAnnotation("Relational:ColumnType", "smalldatetime");

                    b.Property<string>("Description");

                    b.Property<string>("Image");

                    b.Property<string>("Name");

                    b.Property<int?>("PagesCount");

                    b.Property<string>("Publisher");

                    b.Property<int?>("Year");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("LibraryWebApp.Models.BookLend", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Count");

                    b.Property<DateTime?>("DateLend")
                        .HasAnnotation("Relational:ColumnType", "smalldatetime");

                    b.Property<DateTime?>("DateReturn")
                        .HasAnnotation("Relational:ColumnType", "smalldatetime");

                    b.Property<int>("IdBook");

                    b.Property<string>("IdUser")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 450);

                    b.HasKey("Id");
                });

            modelBuilder.Entity("LibraryWebApp.Models.Category", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("LibraryWebApp.Models.WishList", b =>
                {
                    b.Property<string>("IdUser")
                        .HasAnnotation("MaxLength", 450);

                    b.Property<int>("IdBook");

                    b.Property<DateTime?>("DateAdd")
                        .HasAnnotation("Relational:ColumnType", "smalldatetime");

                    b.HasKey("IdUser", "IdBook");
                });

            modelBuilder.Entity("LibraryWebApp.Models.AspNetRoleClaims", b =>
                {
                    b.HasOne("LibraryWebApp.Models.AspNetRoles")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("LibraryWebApp.Models.AspNetUserClaims", b =>
                {
                    b.HasOne("LibraryWebApp.Models.AspNetUsers")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("LibraryWebApp.Models.AspNetUserLogins", b =>
                {
                    b.HasOne("LibraryWebApp.Models.AspNetUsers")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("LibraryWebApp.Models.AspNetUserRoles", b =>
                {
                    b.HasOne("LibraryWebApp.Models.AspNetRoles")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("LibraryWebApp.Models.AspNetUsers")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("LibraryWebApp.Models.Book", b =>
                {
                    b.HasOne("LibraryWebApp.Models.Category")
                        .WithMany()
                        .HasForeignKey("CategoryId");
                });

            modelBuilder.Entity("LibraryWebApp.Models.BookLend", b =>
                {
                    b.HasOne("LibraryWebApp.Models.Book")
                        .WithMany()
                        .HasForeignKey("IdBook");

                    b.HasOne("LibraryWebApp.Models.AspNetUsers")
                        .WithMany()
                        .HasForeignKey("IdUser");
                });

            modelBuilder.Entity("LibraryWebApp.Models.WishList", b =>
                {
                    b.HasOne("LibraryWebApp.Models.Book")
                        .WithMany()
                        .HasForeignKey("IdBook");

                    b.HasOne("LibraryWebApp.Models.AspNetUsers")
                        .WithMany()
                        .HasForeignKey("IdUser");
                });
        }
    }
}
