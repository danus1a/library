﻿using System.Linq;
using System.Threading.Tasks;
using LibraryWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LibraryWebApp.ViewComponents
{
    [ViewComponent]
    public class Categories:ViewComponent
    {
        private LibraryContext _libraryContext;

        public Categories(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }

        public async Task<IViewComponentResult> InvokeAsync(int count)
        {
            var categories = await _libraryContext.Category.ToListAsync();
            if (count > 0)
            {
                return View(categories.Take(count));
            }
            return View(categories);
        }
    }
}
