﻿using System.Threading.Tasks;
using LibraryWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LibraryWebApp.ViewComponents
{
    [ViewComponent]
    public class BooksInUse: ViewComponent
    {
        private LibraryContext _libraryContext;

        public BooksInUse(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user =
                await
                    _libraryContext.AspNetUsers.Include(u => u.BookLend)
                        .FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            return View(user);
        }
    }
}
