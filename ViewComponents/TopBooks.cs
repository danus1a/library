﻿using LibraryWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryWebApp.ViewComponents
{
    [ViewComponent]
    public class TopBooks:ViewComponent
    {
        private LibraryContext _libraryContext;

        public TopBooks(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }
        public async Task<IViewComponentResult> InvokeAsync(int numberOfItems)
        {
            var topBooks = await _libraryContext.Book.Include(b => b.WishList).Include(b => b.Category).OrderByDescending(b => b.BookLend.Count).ToListAsync();
            return View(topBooks.Take(numberOfItems));
        }
    }
}
