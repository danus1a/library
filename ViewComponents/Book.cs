﻿using System.Threading.Tasks;
using LibraryWebApp.Models;
using LibraryWebApp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LibraryWebApp.ViewComponents
{
    [ViewComponent]
    public class Book : ViewComponent
    {
        private LibraryContext _libraryContext;

        public Book(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }

        public async Task<IViewComponentResult> InvokeAsync(Models.Book book)
        {
            var user = await _libraryContext.AspNetUsers.Include(u=>u.WishList).FirstOrDefaultAsync(u=>u.UserName == User.Identity.Name);
            var userBooksModel = new UserBooksViewModel
            {
                User = user,
                Book = book
            };
            return View(userBooksModel);
        }
    }
}
