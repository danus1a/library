﻿using LibraryWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryWebApp.ViewComponents
{
    [ViewComponent]
    public class NewBooks : ViewComponent
    {
        private LibraryContext _libraryContext;

        public NewBooks(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }
        public async Task<IViewComponentResult> InvokeAsync(int numberOfItems)
        {
            var newBooks = await _libraryContext.Book.Include(b => b.WishList).Include(b => b.Category).OrderByDescending(book => book.DateAdded).ToListAsync();
            return View(newBooks.Take(numberOfItems));
        }
    }
}
