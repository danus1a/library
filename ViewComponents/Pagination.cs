﻿using System.Threading.Tasks;
using LibraryWebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace LibraryWebApp.ViewComponents
{
    [ViewComponent]
    public class Pagination: ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(Pager pager){
            return View(pager);
        }
    }
}
