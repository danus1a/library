using System;

namespace LibraryWebApp.Models
{
    public partial class WishList
    {
        public string IdUser { get; set; }
        public int IdBook { get; set; }
        public DateTime? DateAdd { get; set; }

        public virtual Book IdBookNavigation { get; set; }
        public virtual AspNetUsers IdUserNavigation { get; set; }
    }
}
