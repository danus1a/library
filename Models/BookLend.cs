using System;

namespace LibraryWebApp.Models
{
    public partial class BookLend
    {
        public int Id { get; set; }
        public int Count { get; set; }
        public DateTime? DateLend { get; set; }
        public DateTime? DateReturn { get; set; }
        public int IdBook { get; set; }
        public string IdUser { get; set; }

        public virtual Book IdBookNavigation { get; set; }
        public virtual AspNetUsers IdUserNavigation { get; set; }
    }
}
