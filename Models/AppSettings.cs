﻿namespace LibraryWebApp.Models
{
    public class AppSettings
    {
        public string AdminEmail { get; set; }
        public string ConnectionString { get; set; }
    }
}