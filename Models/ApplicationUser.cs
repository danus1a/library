﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace LibraryWebApp.Models
{
    public class ApplicationUser:IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SecondName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }
}
