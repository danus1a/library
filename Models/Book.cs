using System;
using System.Collections.Generic;

namespace LibraryWebApp.Models
{
    public partial class Book
    {
        public Book()
        {
            BookLend = new HashSet<BookLend>();
            WishList = new HashSet<WishList>();
        }

        public int Id { get; set; }
        public string Author { get; set; }
        public int? CategoryId { get; set; }
        public string City { get; set; }
        public int? Count { get; set; }
        public DateTime? DateAdded { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public int? PagesCount { get; set; }
        public string Publisher { get; set; }
        public int? Year { get; set; }

        public virtual ICollection<BookLend> BookLend { get; set; }
        public virtual ICollection<WishList> WishList { get; set; }
        public virtual Category Category { get; set; }
    }
}
