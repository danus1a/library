﻿using LibraryWebApp.Models;
using LibraryWebApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace LibraryWebApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _libraryManager;
        private readonly SignInManager<ApplicationUser> _loginManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly LibraryContext _libraryContext;
        private readonly ILogger<AccountController> _logger;

        public AccountController(UserManager<ApplicationUser> libraryManager, 
                                 SignInManager<ApplicationUser> loginManager, 
                                 RoleManager<IdentityRole> roleManager, 
                                 LibraryContext libraryContext,
                                 ILogger<AccountController> logger)
        {
            _libraryManager = libraryManager;
            _loginManager = loginManager;
            _roleManager = roleManager;
            _libraryContext = libraryContext;
            _logger = logger;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        private async Task<IdentityResult> AddRole(string roleName)
        {
            return await _roleManager.CreateAsync(new IdentityRole { Name = roleName });
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Login,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    SecondName = model.SecondName
                };
                var result = await _libraryManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var roleResult = await _libraryManager.AddToRoleAsync(user, "User");
                    if (roleResult.Succeeded)
                    {
                        //await _loginManager.SignInAsync(user, isPersistent: false);
                        return RedirectToAction("Index", "UsersAdmin");
                    }
                }
                else 
                {
                    foreach (var item in result.Errors)
                    {
                        ModelState.AddModelError(item.Code, item.Description);   
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("PersonalCabinet", "Account");
            }
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [Authorize]
        public async Task<IActionResult> PersonalCabinet()
        {
            var user = await _libraryContext.AspNetUsers.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            var userViewModel = new UserViewModel
            {
                Id = user.Id,
                Address = user.Address,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                SecondName = user.SecondName
            };
            return View(userViewModel);
        }

        [Authorize]
        public async Task<IActionResult> EditPersonalData()
        {
            var user = await _libraryContext.AspNetUsers.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            var userViewModel = new UserViewModel
            {
                Id = user.Id,
                Address = user.Address,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                SecondName = user.SecondName
            };
            return View(userViewModel);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPersonalData(UserViewModel userViewModel)
        {
            var user = await _libraryContext.AspNetUsers.FirstOrDefaultAsync(u => u.Id == userViewModel.Id);
            if (user == null)
                return BadRequest();
            if (ModelState.IsValid)
            {
                user.FirstName = userViewModel.FirstName;
                user.LastName = userViewModel.LastName;
                user.SecondName = userViewModel.SecondName;
                user.PhoneNumber = userViewModel.PhoneNumber;
                user.Email = userViewModel.Email;
                user.Address = userViewModel.Address;
                await _libraryContext.SaveChangesAsync();
                return RedirectToAction("PersonalCabinet");
            }
            return View(userViewModel);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var result = await _loginManager.PasswordSignInAsync(model.Login,
                    model.Password, false, false);
                if (result.Succeeded)
                {
                    if (string.IsNullOrWhiteSpace(returnUrl))
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    return RedirectToReturnUrl(returnUrl);
                }
                ModelState.AddModelError("", "Username or password incorrect");
            }
            return View(model);
        }

        public async Task<IActionResult> LogOff()
        {
            if (User.Identity.IsAuthenticated)
            {
                HttpContext.Session.Remove("IsAdmin");
                await _loginManager.SignOutAsync();
            }
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        private IActionResult RedirectToReturnUrl(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
        [Authorize]
        public async Task<IActionResult> Wishlist()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = await _libraryContext.AspNetUsers.Include(u=>u.WishList).ThenInclude(w => w.IdBookNavigation).FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                if (user != null)
                {
                    return View(user.WishList);
                }
                return NotFound();
            }
            return NotFound();
        }

        [Authorize]
        public async Task<IActionResult> BooksHistory()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = await _libraryContext.AspNetUsers.Include(u => u.BookLend).ThenInclude(b=>b.IdBookNavigation).FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                if (user != null)
                {
                    return View(user.BookLend);
                }
                return NotFound();
            }
            return NotFound();
        }
    }
}