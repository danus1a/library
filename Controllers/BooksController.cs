using LibraryWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace LibraryWebApp.Controllers
{
    public class BooksController : Controller
    {
        private const int _productsOnPage = 5;
        private const int _pagesOnPagination = 5;
        private LibraryContext _libraryContext;
        private ILogger<BooksController> _logger;

        public BooksController(LibraryContext context, ILogger<BooksController> logger)
        {
            _libraryContext = context;
            _logger = logger;
        }

        // GET: Books
        //pages from 1 to 5
        public async Task<IActionResult> All(int? page)
        {
            ViewBag.Title = "Books";
            var books = await _libraryContext.Book.Include(b => b.Category).Include(u => u.WishList).ToListAsync();
            return GetPagination(page, books, "All", null);
        }

        private IActionResult GetPagination(int? page, List<Book> books, string action, string routeCategory)
        {
            if (!books.Any())
            {
                return View("Books", books);
            }
            if (page == null || page.Value <= 0)
                page = 1;
            var pagesCount = Math.Ceiling((decimal) books.Count/_productsOnPage);
            if (page.Value > pagesCount)
                return BadRequest();
            var pager = new Pager
            {
                PreviousPage = page.Value >= 2 ? (page.Value - 1).ToString() : "",
                NextPage = page.Value < pagesCount ? (page.Value + 1).ToString() : "",
                CurrentPage = page.Value.ToString(),
                Action = action,
                RouteCategory = routeCategory
            };
            //current page
            var pagesList = new List<string> {page.Value.ToString()};
            if (page.Value == 1)
            {
                for (var i = page.Value + 1; i <= _pagesOnPagination; i++)
                {
                    if (i > pagesCount)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == 2)
            {
                pagesList.Add((page.Value - 1).ToString());
                for (var i = page.Value + 1; i <= _pagesOnPagination; i++)
                {
                    if (i > pagesCount)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == pagesCount)
            {
                for (var i = page.Value - 1; i > 0; i--)
                {
                    if (pagesList.Count == _pagesOnPagination)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == pagesCount - 1)
            {
                pagesList.Add(pagesCount.ToString());
                for (var i = page.Value - 1; i > 0; i--)
                {
                    if (pagesList.Count == _pagesOnPagination)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == pagesCount - 2)
            {
                pagesList.Add(pagesCount.ToString());
                pagesList.Add((pagesCount - 1).ToString());
                for (var i = page.Value - 1; i > 0; i--)
                {
                    if (pagesList.Count == _pagesOnPagination)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else
            {
                for (var i = 1; i <= _pagesOnPagination; i++)
                {
                    if (i != page.Value)
                        pagesList.Add(i.ToString());
                }
            }
            pager.Pages = pagesList.OrderBy(item => item).ToList();
            ViewBag.pager = pager;
            return View("Books", books.Skip((page.Value - 1)*_productsOnPage).Take(_productsOnPage));
        }

        public async Task<IActionResult> Top()
        {
            var books =
                await
                    _libraryContext.Book.Include(b => b.Category).Include(b => b.WishList).OrderByDescending(b => b.BookLend.Count).ToListAsync();
            return View(books.Take(10));
        }

        public async Task<IActionResult> New()
        {
            var books =
                await
                    _libraryContext.Book.Include(b => b.WishList).Include(b => b.Category)
                        .OrderByDescending(book => book.DateAdded).Take(10)
                        .ToListAsync();
            return View(books);
        }

        public async Task<IActionResult> Category(int? id, int? page)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var category = _libraryContext.Category.FirstOrDefaultAsync(c => c.Id == id).Result;
            if (category == null)
            {
                return BadRequest();
            }
            var books = await _libraryContext.Book.Include(b => b.WishList).Where(b => b.CategoryId == id).ToListAsync();
            ViewBag.Title = category.Name;
            ViewBag.Category = ViewBag.Title;
            ViewBag.CategoryId = category.Id;
            return GetPagination(page, books, "Category", id.ToString());
        }

        // GET: Books/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Book book = await _libraryContext.Book.Include(b => b.Category).SingleOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }
            var booksLend =
                await _libraryContext.BookLend.Where(b => b.IdBook == id && !b.DateReturn.HasValue).ToListAsync();
            ViewBag.BooksLeft = book.Count - booksLend.Count;
            return View(book);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddToWishListPost(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var adding = await AddToWishList(id);
                if (adding)
                {
                    return RedirectToAction("WishList", "Account");
                }
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddToWishListAjax(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var adding = await AddToWishList(id);
                if (adding)
                {
                    if (ViewData["book_temp"] != null)
                    {
                        var book = (Book)ViewData["book_temp"];
                        return ViewComponent("Book", new {book = book});
                    }
                }
            }
            return BadRequest();
        }
        [Authorize]
        public async Task<bool> AddToWishList(int id)
        {
            try
            {
                var user = await _libraryContext.AspNetUsers.Include(u => u.WishList)
                    .FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                var book = await _libraryContext.Book.Include(b => b.Category).Include(b => b.WishList)
                    .FirstOrDefaultAsync(b => b.Id == id);
                if (user != null && book != null)
                {
                    user.WishList.Add(new WishList
                    {
                        DateAdd = DateTime.Now,
                        IdBook = id
                    });
                    await _libraryContext.SaveChangesAsync();
                    ViewData["book_temp"] = book;
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("AddToWishList book #{0} - {1}", id, ex.Message);
            }
            return false;
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> RemoveFromWishListPost(int idBook)
        {
            if (User.Identity.IsAuthenticated)
            {
                var removing = await RemoveFromWishList(idBook);
                if (removing)
                {
                    return RedirectToAction("WishList", "Account");
                }
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> RemoveFromWishListAjax(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var removing = await RemoveFromWishList(id);
                if (removing)
                {
                    if (ViewData["book_temp"] != null)
                    {
                        var book = (Book) ViewData["book_temp"];
                        return ViewComponent("Book", new{ book = book});
                    }
                }
            }
            return BadRequest();
        }

        [Authorize]
        public async Task<bool> RemoveFromWishList(int id)
        {
            try
            {
                var user = await _libraryContext.AspNetUsers.Include(u => u.WishList)
                    .FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                var book = await _libraryContext.Book.Include(b => b.Category)
                    .Include(b => b.WishList).FirstOrDefaultAsync(b => b.Id == id);
                if (user != null && book != null)
                {
                    var wishBook = user.WishList.FirstOrDefault(w => w.IdBook == id);
                    user.WishList.Remove(wishBook);
                    await _libraryContext.SaveChangesAsync();
                    ViewData["book_temp"] = book;
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("RemoveFromWishList book #{0} - {1}", id, ex.Message);
            }
            return false;
        }
    }
}