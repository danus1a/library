﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LibraryWebApp.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        [HttpGet()]
        public IActionResult Index()
        {
            return RedirectToAction("Index", "BooksAdmin");
        }
    }
}