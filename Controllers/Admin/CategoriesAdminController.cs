using System.Linq;
using System.Threading.Tasks;
using LibraryWebApp.Models;
using LibraryWebApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LibraryWebApp.Controllers.Admin
{
    [Authorize(Roles="Admin")]
    [Route("Admin/Categories")]
    public class CategoriesAdminController : Controller
    {
        private LibraryContext _libraryContext;

        public CategoriesAdminController(LibraryContext context)
        {
            _libraryContext = context;    
        }

        // GET: CategoriesAdmin
        [HttpGet()]
        [HttpGet("Index")]
        [HttpGet("All")]
        public async Task<IActionResult> Index()
        {
            var categories = await _libraryContext.Category.Include(c=>c.Book).ToListAsync();
            return View(categories);
        }

        // GET: CategoriesAdmin/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: CategoriesAdmin/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CategoryViewModel categoryViewModel)
        {
            if (ModelState.IsValid)
            {
                if (!_libraryContext.Category.AnyAsync(category =>
                    category.Name == categoryViewModel.Name).Result)
                {
                    _libraryContext.Category.Add(new Category
                    {
                        Name = categoryViewModel.Name
                    });
                    await _libraryContext.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("Name", "Name already exists");
            }
            return View(categoryViewModel);
        }

        // GET: CategoriesAdmin/Edit/5
        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Category category = await _libraryContext.Category.SingleOrDefaultAsync(m => m.Id == id);
            if (category == null)
            {
                return NotFound();
            }
            var categoryViewModel = new CategoryViewModel
            {
                Id = category.Id,
                Name = category.Name
            };
            return View(categoryViewModel);
        }

        [HttpGet("{id}/Books")]
        public async Task<IActionResult> Books(int? id)
        {
            if (id==null)
            {
                return NotFound();
            }
            Category category = await _libraryContext.Category.SingleOrDefaultAsync((m => m.Id == id));
            if (category == null)
            {
                return NotFound();
            }
            var books = _libraryContext.Book.ToList().Where(book => book.CategoryId == id);
            ViewData["CategoryName"] = category.Name;
            return View("Books", books);
        }

        // POST: CategoriesAdmin/Edit/5
        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditCategory(CategoryViewModel categoryViewModel)
        {
            Category category = await _libraryContext.Category.SingleOrDefaultAsync(m => m.Id == categoryViewModel.Id);
            if (category == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (!_libraryContext.Category.AnyAsync(c =>
                    c.Name == categoryViewModel.Name).Result)
                {
                    category.Name = categoryViewModel.Name;
                    _libraryContext.Update(category);
                    await _libraryContext.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("Name", "Name already exists");
            }
            return View(category);
        }

        // GET: CategoriesAdmin/Delete/5
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Category category = await _libraryContext.Category.SingleOrDefaultAsync(m => m.Id == id);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // POST: CategoriesAdmin/Delete/5
        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Category category = await _libraryContext.Category.SingleOrDefaultAsync(m => m.Id == id);
            _libraryContext.Category.Remove(category);
            await _libraryContext.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
