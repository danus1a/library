using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LibraryWebApp.Models;
using LibraryWebApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Net.Http.Headers;

namespace LibraryWebApp.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/Books")]
    public class BooksAdminController : Controller
    {
        private readonly LibraryContext _libraryContext;
        private IHostingEnvironment _environment;
        private string[] _allowedExtensions;

        public BooksAdminController(LibraryContext context, IHostingEnvironment environment)
        {
            _libraryContext = context;
            _environment = environment;
            _allowedExtensions = new[] { ".jpg", ".png", ".gif" };
        }

        // GET: Books
        [HttpGet()]
        [HttpGet("Index")]
        [HttpGet("All")]
        public async Task<IActionResult> Index()
        {
            var books = _libraryContext.Book.Include(b => b.Category);
            var booksViewModel = new List<BookViewModel>();
            var booksInUse = await _libraryContext.BookLend.Where(b => !b.DateReturn.HasValue).GroupBy(b => b.IdBook).ToListAsync();
            foreach (var book in books)
            {
                var booksAvailable = book.Count;
                var currentBookInUse = booksInUse.FirstOrDefault(b => b.Key == book.Id);
                if (currentBookInUse != null)
                    booksAvailable = book.Count - currentBookInUse.Sum(b => b.Count);
                booksViewModel.Add(new BookViewModel
                {
                    Author = book.Author,
                    CategoryId = book.CategoryId,
                    CategoryName = book.Category.Name,
                    City = book.City,
                    Count = book.Count,
                    DateAdded = book.DateAdded.HasValue ? book.DateAdded.Value.ToString("dd.MM.yyyy") : "",
                    Description = book.Description,
                    Id = book.Id,
                    ImageUrl = book.Image,
                    Name = book.Name,
                    PagesCount = book.PagesCount,
                    Publisher = book.Publisher,
                    Year = book.Year,
                    Available = booksAvailable
                });
            }
            return View(booksViewModel);
        }

        // GET: Books/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            ViewData["CategoryId"] = GetCategoriesSelectList();
            return View();
        }

        private List<SelectListItem> GetCategoriesSelectList(string selectedValue = null)
        {
            return _libraryContext.Category.Select(item => new SelectListItem
            {
                Value = item.Id.ToString(),
                Text = item.Name,
                Selected = item.Id.ToString() == selectedValue
            }).ToList();
        }

        private bool VerifyFileExtension(string path)
        {
            return _allowedExtensions.Contains(Path.GetExtension(path));
        }

        // POST: Books/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BookViewModel bookViewModel)
        {
            if (ModelState.IsValid)
            {
                var fileName = "";
                if (bookViewModel.Image != null && bookViewModel.Image.Length > 0)
                {
                    fileName =
                        ContentDispositionHeaderValue.Parse(bookViewModel.Image.ContentDisposition).FileName.Trim('"');
                    if (VerifyFileExtension(fileName))
                    {
                        var uploads = Path.Combine(_environment.WebRootPath, "img/Books");
                        using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                        {
                            await bookViewModel.Image.CopyToAsync(fileStream);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Image",
                            $"Image can be only with extensions: {_allowedExtensions.Join()}.");
                        ViewData["CategoryId"] = GetCategoriesSelectList(bookViewModel.CategoryId.ToString());
                        return View(bookViewModel);
                    }
                }
                var book = new Book
                {
                    Author = bookViewModel.Author,
                    Name = bookViewModel.Name,
                    DateAdded = null,
                    Description = bookViewModel.Description,
                    CategoryId = bookViewModel.CategoryId,
                    City = bookViewModel.City,
                    Count = bookViewModel.Count,
                    Image = fileName==""?null:$"~/img/Books/{fileName}",
                    PagesCount = bookViewModel.PagesCount,
                    Publisher = bookViewModel.Publisher,
                    Year = bookViewModel.Year
                };
                DateTime dateAdded;
                if(DateTime.TryParseExact(bookViewModel.DateAdded, "dd.MM.yyyy", null, DateTimeStyles.None, out dateAdded))
                {
                    book.DateAdded = dateAdded;
                }
                _libraryContext.Book.Add(book);
                await _libraryContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["CategoryId"] = GetCategoriesSelectList(bookViewModel.CategoryId.ToString());
            return View(bookViewModel);
        }

        // GET: Books/Edit/5
        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Book book = await _libraryContext.Book.SingleOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }
            var bookViewModel = new BookViewModel
            {
                Name = book.Name,
                DateAdded = book.DateAdded.HasValue ? book.DateAdded.Value.ToString("dd.MM.yyyy") : "",
                Id = book.Id,
                Description = book.Description,
                Author = book.Author,
                CategoryId = book.CategoryId,
                City = book.City,
                Count = book.Count,
                ImageUrl = book.Image,
                PagesCount = book.PagesCount,
                Publisher = book.Publisher,
                Year = book.Year
            };
            ViewData["CategoryId"] = GetCategoriesSelectList(book.CategoryId.ToString());

            return View(bookViewModel);
        }

        // POST: Books/Edit/5
        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBook(BookViewModel bookViewModel)
        {
            Book book = await _libraryContext.Book.SingleOrDefaultAsync(m => m.Id == bookViewModel.Id);
            if (book == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                var fileName = "";
                if (bookViewModel.Image != null && bookViewModel.Image.Length > 0)
                {
                    fileName = ContentDispositionHeaderValue.Parse(bookViewModel.Image.ContentDisposition).FileName.Trim('"');
                    if (VerifyFileExtension(fileName))
                    {
                        var uploads = Path.Combine(_environment.WebRootPath, "img/Books");
                        using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                        {
                            await bookViewModel.Image.CopyToAsync(fileStream);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Image",
                            $"Image can be only with extensions: {_allowedExtensions.Join()}.");
                        ViewData["CategoryId"] = GetCategoriesSelectList(book.CategoryId.ToString());
                        return View(bookViewModel);
                    }
                }
                book.Author = bookViewModel.Author;
                book.Name = bookViewModel.Name;
                book.Description = bookViewModel.Description;
                book.CategoryId = bookViewModel.CategoryId;
                book.City = bookViewModel.City;
                book.Count = bookViewModel.Count;
                if(fileName!="")
                    book.Image = $"~/img/Books/{fileName}";
                book.PagesCount = bookViewModel.PagesCount;
                book.Publisher = bookViewModel.Publisher;
                book.Year = bookViewModel.Year;                
                DateTime dateAdded;
                if(DateTime.TryParseExact(bookViewModel.DateAdded, "dd.MM.yyyy", null, DateTimeStyles.None, out dateAdded))
                {
                    book.DateAdded = dateAdded;
                }
                _libraryContext.Update(book);
                await _libraryContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["CategoryId"] = GetCategoriesSelectList(book.CategoryId.ToString());
            return View("Edit",bookViewModel);
        }

        // GET: Books/Delete/5
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Book book = await _libraryContext.Book.SingleOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Book book = await _libraryContext.Book.SingleOrDefaultAsync(m => m.Id == id);
            _libraryContext.Book.Remove(book);
            await _libraryContext.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
