using System.Linq;
using System.Threading.Tasks;
using LibraryWebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace LibraryWebApp.Controllers.Admin
{
    [Authorize(Roles="Admin")]
    [Route("Admin/Users")]
    public class UsersAdminController : Controller
    {
        private LibraryContext _libraryContext;
        private string _adminEmail;

        public UsersAdminController(LibraryContext context, IOptions<AppSettings> settings)
        {
            _libraryContext = context;
            _adminEmail = settings.Value.AdminEmail;
        }

        // GET: UsersAdmin
        [HttpGet()]
        [HttpGet("Index")]
        [HttpGet("All")]
        public async Task<IActionResult> Index()
        {
            return View(await _libraryContext.AspNetUsers.Where(user=>user.UserName!= _adminEmail).ToListAsync());
        }

        // GET: UsersAdmin/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            return View("Register");
        }

        // GET: UsersAdmin/Delete/5
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AspNetUsers aspNetUsers = await _libraryContext.AspNetUsers.SingleOrDefaultAsync(m => m.Id == id);
            if (aspNetUsers == null)
            {
                return NotFound();
            }

            return View(aspNetUsers);
        }

        // POST: UsersAdmin/Delete/5
        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            AspNetUsers aspNetUsers = await _libraryContext.AspNetUsers.SingleOrDefaultAsync(m => m.Id == id);
            _libraryContext.AspNetUsers.Remove(aspNetUsers);
            await _libraryContext.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
