using System.Collections.Generic;
using System.Threading.Tasks;
using LibraryWebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using LibraryWebApp.ViewModels;
using System;
using System.Globalization;

namespace LibraryWebApp.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/BookLend")]
    public class BookLendsAdminController : Controller
    {
        private LibraryContext _libraryContext;

        public BookLendsAdminController(LibraryContext context)
        {
            _libraryContext = context;    
        }

        // GET: BookLends
        [HttpGet()]
        [HttpGet("Index")]
        [HttpGet("All")]
        public async Task<IActionResult> Index()
        {
            var books = _libraryContext.BookLend.Include(b => b.IdBookNavigation).Include(b => b.IdUserNavigation);
            return View(await books.OrderBy(b => b.DateLend.Value).ToListAsync());
        }
        // GET: BookLends filtered by book
        [HttpGet("FilteredBook/{id}")]
        public async Task<IActionResult> FilteredBook(int? id)
        {
            if (id == null)
                return BadRequest();
            var booksFiltered = _libraryContext.BookLend.Where(b=>b.IdBook==id).Include(b => b.IdBookNavigation).Include(b => b.IdUserNavigation);
            return View("Index", await booksFiltered.ToListAsync());
        }
        // GET: BookLends filtered by user
        [HttpGet("FilteredUser/{id}")]
        public async Task<IActionResult> FilteredUser(string id)
        {
            if (id == null)
                return BadRequest();
            var booksFiltered = _libraryContext.BookLend.Where(b => b.IdUser == id).Include(b => b.IdBookNavigation).Include(b => b.IdUserNavigation);
            return View("Index", await booksFiltered.ToListAsync());
        }

        // GET: BookLends/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            var booksSelect = GetBooksSelectList(false);
            ViewData["BookId"] = booksSelect;
            ViewData["UserId"] = GetUsersSelectList();
            if(booksSelect.Any())
                ViewData["BooksCount"] = GetBooksCountSelectList(booksSelect.FirstOrDefault().Value, "", null, 0);
            return View();
        }

        // POST: BookLends/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BookLendViewModel bookLendViewModel)
        {
            if (ModelState.IsValid)
            {
                var bookLend = new BookLend
                {
                    DateLend = null, 
                    DateReturn = null,
                    IdBook = bookLendViewModel.IdBook,
                    IdUser = bookLendViewModel.IdUser,
                    Count = bookLendViewModel.Count
                };
                DateTime dateLend, dateReturn; 
                if (DateTime.TryParseExact(bookLendViewModel.DateLend, "dd.MM.yyyy HH:mm", null, DateTimeStyles.None, out dateLend))
                {
                    bookLend.DateLend = dateLend;
                }
                if (DateTime.TryParseExact(bookLendViewModel.DateReturn, "dd.MM.yyyy HH:mm", null, DateTimeStyles.None, out dateReturn))
                {
                    bookLend.DateReturn = dateReturn;
                }
                _libraryContext.BookLend.Add(bookLend);
                await _libraryContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var booksSelect = GetBooksSelectList(false, bookLendViewModel.IdBook.ToString());
            ViewData["BookId"] = booksSelect;
            ViewData["UserId"] = GetUsersSelectList(bookLendViewModel.IdUser);
            if (booksSelect.Any())
                ViewData["BooksCount"] = GetBooksCountSelectList(bookLendViewModel.IdBook.ToString(), bookLendViewModel.Count.ToString(), null, 0);
            return View(bookLendViewModel);
        }

        // GET: BookLends/Edit/5
        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            BookLend bookLend = await _libraryContext.BookLend.SingleOrDefaultAsync(m => m.Id == id);
            if (bookLend == null)
            {
                return NotFound();
            }
            var bookLendViewModel = new BookLendViewModel
            {
                Id = bookLend.Id,
                DateLend = bookLend.DateLend.HasValue ? bookLend.DateLend.Value.ToString("dd.MM.yyyy HH:mm") : "",
                DateReturn = bookLend.DateReturn.HasValue ? bookLend.DateReturn.Value.ToString("dd.MM.yyyy HH:mm") : "",
                IdBook = bookLend.IdBook,
                IdUser = bookLend.IdUser,
                Count = bookLend.Count
            };
            var booksSelect = GetBooksSelectList(true, bookLendViewModel.IdBook.ToString());
            ViewData["BookId"] = booksSelect;
            ViewData["UserId"] = GetUsersSelectList(bookLendViewModel.IdUser);
            if (booksSelect.Any())
                ViewData["BooksCount"] = GetBooksCountSelectList(bookLendViewModel.IdBook.ToString(), bookLendViewModel.Count.ToString(), bookLendViewModel.IdBook.ToString(), bookLendViewModel.Count);
            return View(bookLendViewModel);
        }

        // POST: BookLends/Edit/5
        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBook(BookLendViewModel bookLendViewModel)
        {
            BookLend bookLend = await _libraryContext.BookLend.SingleOrDefaultAsync(b => b.Id == bookLendViewModel.Id);
            if (bookLend == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                bookLend.IdBook = bookLendViewModel.IdBook;
                bookLend.IdUser = bookLendViewModel.IdUser;
                bookLend.Count = bookLendViewModel.Count;
                DateTime dateLend, dateReturn; 
                if (DateTime.TryParseExact(bookLendViewModel.DateLend, "dd.MM.yyyy HH:mm", null, DateTimeStyles.None, out dateLend))
                {
                    bookLend.DateLend = dateLend;
                }
                if (DateTime.TryParseExact(bookLendViewModel.DateReturn, "dd.MM.yyyy HH:mm", null, DateTimeStyles.None, out dateReturn))
                {
                    bookLend.DateReturn = dateReturn;
                }
                _libraryContext.Update(bookLend);
                await _libraryContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var booksSelect = GetBooksSelectList(true, bookLendViewModel.IdBook.ToString());
            ViewData["BookId"] = booksSelect;
            ViewData["UserId"] = GetUsersSelectList(bookLendViewModel.IdUser);
            if (booksSelect.Any())
                ViewData["BooksCount"] = GetBooksCountSelectList(bookLendViewModel.IdBook.ToString(), bookLendViewModel.Count.ToString(), bookLendViewModel.IdBook.ToString(), bookLend.Count);
            return View("Edit", bookLendViewModel);
        }

        // GET: BookLends/Delete/5
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            BookLend bookLend = await _libraryContext.BookLend.Include(b=>b.IdUserNavigation).SingleOrDefaultAsync(m => m.Id == id);
            if (bookLend == null)
            {
                return NotFound();
            }

            return View(bookLend);
        }

        // POST: BookLends/Delete/5
        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            BookLend bookLend = await _libraryContext.BookLend.SingleOrDefaultAsync(m => m.Id == id);
            _libraryContext.BookLend.Remove(bookLend);
            await _libraryContext.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        private List<SelectListItem> GetBooksSelectList(bool edit, string selectedValue = null)
        {
            var booksInUse = _libraryContext.BookLend.Where(b=>!b.DateReturn.HasValue).GroupBy(b => b.IdBook).ToList();
            var books = new List<SelectListItem>();
            foreach (var book in _libraryContext.Book)
            {
                var booksCanBeSelected = book.Count;
                var currentBookInUse = booksInUse.FirstOrDefault(b => b.Key == book.Id);
                if (currentBookInUse != null)
                    booksCanBeSelected = book.Count - currentBookInUse.Sum(b=>b.Count);
                if (booksCanBeSelected > 0 || (book.Id.ToString() == selectedValue && edit))
                {
                    books.Add(new SelectListItem
                    {
                        Value = book.Id.ToString(),
                        Text = book.Name,
                        Selected = book.Id.ToString() == selectedValue
                    });
                }
            }
            return books.OrderBy(i=>i.Text).ToList();
        }
        private List<SelectListItem> GetUsersSelectList(string selectedValue = null)
        {
            var list = new List<SelectListItem>();
            foreach (var user in _libraryContext.AspNetUsers)
            {
                list.Add(new SelectListItem
                {
                    Value = user.Id,
                    Text = $"{user.LastName} {user.FirstName} {user.SecondName}",
                    Selected = user.Id==selectedValue
                });
            }
            return list.Where(i=>!string.IsNullOrWhiteSpace(i.Text)).OrderBy(i=>i.Text).ToList();
        }
        [HttpGet("GetBooksCountSelectList")]
        public List<SelectListItem> GetBooksCountSelectList(string idBook, string selectedValue, string currentBook, int currentSelected)
        {
            var list = new List<SelectListItem>();
            int id;
            if (int.TryParse(idBook, out id))
            {
                var book = _libraryContext.Book.FirstOrDefault(b => b.Id == id);
                if (book == null)
                {
                    return list;
                }
                var booksInUse =
                    _libraryContext.BookLend.Where(b => !b.DateReturn.HasValue && b.IdBook == id).ToList();
                var countBooksInUse = booksInUse.Sum(bookLend => bookLend.Count);
                var countAvailable = book.Count - countBooksInUse;
                if (currentBook == idBook)
                    countAvailable += currentSelected;
                for (var i = 1; i <= countAvailable; i++)
                {
                    list.Add(new SelectListItem
                    {
                        Value = i.ToString(),
                        Text = i.ToString(),
                        Selected = i.ToString() == selectedValue
                    });
                }
            }
            return list;
        }
    }
}
