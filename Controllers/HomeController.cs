﻿
using LibraryWebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace LibraryWebApp.Controllers
{
    public class HomeController : Controller
    {
        private LibraryContext _libraryContext;

        public HomeController(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Error()
        {
            return View();
        }
    }
}