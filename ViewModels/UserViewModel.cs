﻿using System.ComponentModel.DataAnnotations;

namespace LibraryWebApp.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string Address { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "First name is required")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name = "Second name")]
        public string SecondName { get; set; }

        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
    }
}