﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace LibraryWebApp.ViewModels
{
    public class BookViewModel
    {
        public int Id { get; set; }

        [MinLength(5, ErrorMessage = "Lenght of the field Author must be at least 5 symbols")]
        [StringLength(100, ErrorMessage = "Maximum lenght of the field Author is 100")]
        [Required(ErrorMessage = "Author is required")]
        public string Author { get; set; }

        [Required(ErrorMessage = "Category is required")]
        [Display(Name = "Category")]
        public int? CategoryId { get; set; }

        [Display(Name = "Category")]
        public string CategoryName { get; set; }

        [MinLength(5, ErrorMessage = "Lenght of the field City must be at least 5 symbols")]
        [StringLength(100, ErrorMessage = "Maximum lenght of the field City is 100")]
        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        [Range(0, 100, ErrorMessage = "Count can be from 0 to 100")]
        [Required(ErrorMessage = "Count is required")]
        public int? Count { get; set; }

        [Required(ErrorMessage = "Date added is required")]
        [Display(Name = "Date added")]
        public string DateAdded { get; set; }

        [StringLength(600, ErrorMessage = "Maximum length of the field Description is 600")]
        [MinLength(5, ErrorMessage = "Lenght of the field Description must be at least 5 symbols")]
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        
        public string ImageUrl { get; set; }
        public IFormFile Image { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Count of pages is required")]
        [Range(0, 10000, ErrorMessage = "Count can be from 0 to 10000")]
        [Display(Name = "Count of pages")]
        public int? PagesCount { get; set; }

        [MinLength(5, ErrorMessage = "Lenght of the field Publisher must be at least 5 symbols")]
        [StringLength(100, ErrorMessage = "Maximum lenght of the field Publisher is 100")]
        [Required(ErrorMessage = "Publisher is required")]
        public string Publisher { get; set; }

        [Required(ErrorMessage = "Year is required")]
        [Range(0, 3000, ErrorMessage = "Year can be from 0 to 3000")]
        public int? Year { get; set; }

        [Display(Name="Books available")]
        public int? Available { get; set; }
    }
}