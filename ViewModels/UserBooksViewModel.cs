﻿using LibraryWebApp.Models;

namespace LibraryWebApp.ViewModels
{
    public class UserBooksViewModel
    {
        public AspNetUsers User { get; set; }
        public Book Book { get; set; }
    }
}
