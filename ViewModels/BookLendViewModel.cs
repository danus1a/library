﻿using System.ComponentModel.DataAnnotations;

namespace LibraryWebApp.ViewModels
{
    public class BookLendViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Date of the lend")]
        [Required(ErrorMessage = "Date of the lend is required")]
        public string DateLend { get; set; }

        [Display(Name = "Date of returning")]
        public string DateReturn { get; set; }

        [Display(Name = "Book")]
        [Required(ErrorMessage = "Select a book")]
        public int IdBook { get; set; }

        [Display(Name = "Person")]
        [Required(ErrorMessage = "Select a person")]
        public string IdUser { get; set; }

        [Display(Name = "Count")]
        [Required(ErrorMessage = "Select count")]
        public int Count { get; set; }
    }
}