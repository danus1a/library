﻿using System.ComponentModel.DataAnnotations;

namespace LibraryWebApp.ViewModels
{
    public class CategoryViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [MinLength(5, ErrorMessage = "Length of the field Name must be at least 5 symbols")]
        [StringLength(100, ErrorMessage = "Maximum length of the field Name is 100")]
        public string Name { get; set; }
    }
}