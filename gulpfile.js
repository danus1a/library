﻿var gulp = require('gulp'),
    concat = require('gulp-concat'),
    cssmin = require('gulp-cssmin'),
    uglify = require('gulp-uglify');

var paths = {
    webroot: "./wwwroot/"
};

paths.bootstrapCss = "./bower_components/bootstrap/dist/css/bootstrap.css";
paths.bootstrapDatepickerCss = "./bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css";
paths.bootstrapDataTablesCss = "./bower_components/DataTables/media/css/dataTables.bootstrap.css";
//paths.sbAdminCss = "./bower_components/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css";
//paths.fontAwesomeCss = "./bower_components/font-awesome/css/font-awesome.css";
//paths.morrisCss = "./bower_components/morrisjs/morris.css";

paths.jqueryJs = "./bower_components/jquery/dist/jquery.js";
paths.bootstrapJs = "./bower_components/bootstrap/dist/js/bootstrap.js";
paths.momentJs = "./bower_components/moment/min/moment.min.js";
paths.bootstrapDatepickerJs = "./bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js";
paths.dataTablesJs = "./bower_components/DataTables/media/js/jquery.dataTables.min.js";
paths.dataTablesBootstrapJs = "./bower_components/DataTables/media/js/dataTables.bootstrap.js";
//paths.raphaelJs = "./bower_components/raphael/raphael.js";
//paths.morrisJs = "./bower_components/morrisjs/morris.js";

//paths.fonts = "./bower_components/font-awesome/fonts/*";

paths.jsDest = paths.webroot + "js";
paths.cssDest = paths.webroot + "css";
paths.fontDest = paths.webroot + "fonts";

gulp.task("min:js", function () {
    return gulp.src([paths.jqueryJs, paths.bootstrapJs, paths.momentJs, paths.bootstrapDatepickerJs, paths.dataTablesJs, paths.dataTablesBootstrapJs])
        .pipe(concat(paths.jsDest + "/min/site.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("copy:js", function () {
    return gulp.src([paths.jqueryJs, paths.bootstrapJs, paths.momentJs, paths.bootstrapDatepickerJs, paths.dataTablesJs, paths.dataTablesBootstrapJs])
        .pipe(gulp.dest(paths.jsDest));
});

gulp.task("min:css", function () {
    return gulp.src([paths.bootstrapCss, paths.bootstrapDatepickerCss, paths.bootstrapDataTablesCss])
        .pipe(concat(paths.cssDest + "/min/site.min.css"))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("copy:css", function () {
    return gulp.src([paths.bootstrapCss, paths.bootstrapDatepickerCss, paths.bootstrapDataTablesCss])
        .pipe(gulp.dest(paths.cssDest));
});

//gulp.task("copy:fonts", function () {
//    return gulp.src([paths.fonts])
//        .pipe(gulp.dest(paths.fontDest));
//});

//gulp.task("min:fonts", function () {
//    return gulp.src([paths.fonts])
//        .pipe(gulp.dest(paths.cssDest + "/min/fonts/"));
//});

gulp.task("min", ["min:js", "min:css"]);
gulp.task("copy", ["copy:js", "copy:css"]);